import React, { Component } from "react";
import { connect } from "react-redux";
import { datCuocAction, playGameAction } from "./Redux/actions/xucXacAction";
import ThongTinTroChoi from "./ThongTinTroChoi";
import XucXac from "./XucXac";
import "./XucXac.css";

class GameXucXac extends Component {
  render() {
    return (
      <div className="game">
        <div className="title-game text-center mt-4 display-4">TÀI XỈU</div>
        <div className="row text-center mt-5">
          <div className="col-4">
            <button
              onClick={() => {
                this.props.datCuoc(true);
              }}
              className="btnGame"
            >
              TÀI
            </button>
          </div>
          <div className="col-4">
            <XucXac />
          </div>
          <div className="col-4">
            <button
              onClick={() => {
                this.props.datCuoc(false);
              }}
              className="btnGame"
            >
              Xỉu
            </button>
          </div>
        </div>
        <div className="thongTinTroChoi text-center">
          <ThongTinTroChoi />
          <button
            onClick={() => {
              this.props.playGame();
            }}
            className="btnPlay"
          >
            Play game
          </button>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    datCuoc: (taiXiu) => {
      //   gủi lên reducer
      dispatch(datCuocAction(taiXiu));
    },
    playGame: () => {
      dispatch(playGameAction());
    },
  };
};
export default connect(null, mapDispatchToProps)(GameXucXac);
