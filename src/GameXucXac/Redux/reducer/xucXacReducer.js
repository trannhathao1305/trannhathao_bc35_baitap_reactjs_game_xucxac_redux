import { DAT_CUOC, PLAY_GAME } from "../constant/xucXacConstant";

let initialState = {
  taiXiu: true, // True: là tài (3 -> 11), false: là xỉu (12 trở lên)
  mangXucXac: [
    { ma: 6, hinhAnh: "./imgXucSac/6.png" },
    { ma: 6, hinhAnh: "./imgXucSac/6.png" },
    { ma: 6, hinhAnh: "./imgXucSac/6.png" },
  ],
  soBanThang: 0,
  tongSoBanChoi: 0,
};

export const xucXacReducer = (state = initialState, action) => {
  switch (action.type) {
    case DAT_CUOC: {
      state.taiXiu = action.taiXiu;
      return { ...state };
    }
    case PLAY_GAME: {
      // B1: xử lý random xúc xắc
      let mangXucXacRandom = [];
      for (let i = 0; i < 3; i++) {
        // Mỗi lần random ra số ngẫu nhiên từ 1 -> 6
        let soRandom = Math.floor(Math.random() * 6) + 1;
        // Tạo ra 1 đối tượng xúc xắc từ số ngẫu nhiên
        let xucXacRandom = {
          ma: soRandom,
          hinhAnh: `./imgXucSac/${soRandom}.png`,
        };
        // push vào mảng xúc xắc ngẩu nhiên
        mangXucXacRandom.push(xucXacRandom);
      }
      //   gán state mangXucXac = mangXucXacRandom
      state.mangXucXac = mangXucXacRandom;
      //   Xử lý tăng số lần bàn chơi
      state.tongSoBanChoi += 1;
      //   Xử lý số bàn thắng
      let tongSoDiem = mangXucXacRandom.reduce((tongDiem, xucXac, index) => {
        return (tongDiem += xucXac.ma);
      }, 0);
      //    xét điều kiện để người dùng thắng game
      if (
        (tongSoDiem > 11 && state.taiXiu === false) ||
        (tongSoDiem <= 11 && state.taiXiu === true)
      ) {
        state.soBanThang += 1;
      }
      return { ...state };
    }
    default:
      return state;
  }
};
